
(function(root, factory) {
    if (typeof exports === "object") {
        module.exports = factory(root);
    } else if (typeof define === "function" && define.amd) {
        define([], factory(root));
    } else {
        root.LazyLoad = factory(root);
    }
})(typeof global !== "undefined" ? global : this.window || this.global, function(root) {

    "use strict";

    const defaults = {
        src: "data-src",
        srcset: "data-srcset",
        selector: ".lazyload"
    };
    const extend = function() {

        let extended = {};
        let deep = false;
        let i = 0;
        let length = arguments.length;
        if (Object.prototype.toString.call(arguments[0]) === "[object Boolean]") {
            deep = arguments[0];
            i++;
        }
        let merge = function(obj) {
            for (let prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    if (deep && Object.prototype.toString.call(obj[prop]) === "[object Object]") {
                        extended[prop] = extend(true, extended[prop], obj[prop]);
                    } else {
                        extended[prop] = obj[prop];
                    }
                }
            }
        };
        for (; i < length; i++) {
            let obj = arguments[i];
            merge(obj);
        }

        return extended;
    };

    function LazyLoad(images, options) {
        this.settings = extend(defaults, options || {});
        this.images = images || document.querySelectorAll(this.settings.selector);
        this.observer = null;
        this.init();
    }

    LazyLoad.prototype = {
        init: function() {
            if (!root.IntersectionObserver) {
                this.loadImages();
                return;
            }

            let self = this;
            let observerConfig = {
                root: null,
                rootMargin: "0px",
                threshold: [0]
            };

            this.observer = new IntersectionObserver(function(entries) {

                for (var i = 0; i < entries.length; i++) {
                    if (entries[i].intersectionRatio > 0) {
                        self.observer.unobserve(entries[i].target);
                        let src = entries[i].target.getAttribute(self.settings.src);
                        let srcset = entries[i].target.getAttribute(self.settings.srcset);
                        if ("img" === entries[i].target.tagName.toLowerCase()) {
                            if (src) {
                                entries[i].target.src = src;
                            }
                            if (srcset) {
                                entries[i].target.srcset = srcset;
                            }
                        } else {
                            entries[i].target.style.backgroundImage = "url(" + src + ")";
                        }
                    }
                }
            }, observerConfig);
            for (var i = 0; i < this.images.length; i++) {
                self.observer.observe(this.images[i]);
            }
        },

        loadAndDestroy: function() {
            if (!this.settings) { return; }
            this.loadImages();
            this.destroy();
        },

        loadImages: function() {
            if (!this.settings) { return; }

            let self = this;


            for (var i = 0; i < this.images.length; i++) {

                let src = this.images[i].getAttribute(self.settings.src);
                let srcset = this.images[i].getAttribute(self.settings.srcset);
                if ("img" === this.images[i].tagName.toLowerCase()) {
                    if (src) {
                        this.images[i].src = src;
                    }
                    if (srcset) {
                        this.images[i].srcset = srcset;
                    }
                } else {
                    this.images[i].style.backgroundImage = "url(" + src + ")";
                }
            }
        },

        destroy: function() {
            if (!this.settings) { return; }
            this.observer.disconnect();
            this.settings = null;
        }
    };

    root.lazyload = function(images, options) {
        return new LazyLoad(images, options);
    };

    if (root.jQuery) {
        const $ = root.jQuery;
        $.fn.lazyload = function(options) {
            options = options || {};
            options.attribute = options.attribute || "data-src";
            new LazyLoad($.makeArray(this), options);
            return this;
        };
    }

    return LazyLoad;
});